<?php

namespace Drupal\keysave\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SearchElevateSettingsForm.
 *
 * @ingroup keysave
 */
class KeysaveSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'keysave.settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'keysave_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('keysave.settings');
    foreach ($form_state->getValues() as $key => $value) {
      if (in_array($key, ['include_forms', 'exclude_forms'])) {
        $list = (is_array($value)) ? $value : explode("\n", $value);
        $list = array_map('trim', $list);
        $config->set($key, $list);
      }
    }
    $config->save();
    $this->messenger()->addMessage($this->t('Configuration was saved.'));
  }

  /**
   * Defines the settings form for Search elevate entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('keysave.settings');
    $include = $config->get('include_forms') ?? [];
    $form['include_forms'] = [
      '#type' => 'textarea',
      '#rows' => 8,
      '#title' => $this->t('Forms to Include'),
      '#default_value' => implode("\n", $include),
      '#description' => $this->t('Forms which should have the keysave capability added, each on a separate line.'),
      '#required' => FALSE,
    ];
    $exclude = $config->get('exclude_forms') ?? [];
    $form['exclude_forms'] = [
      '#type' => 'textarea',
      '#rows' => 8,
      '#title' => $this->t('Forms to Exclude'),
      '#default_value' => implode("\n", $exclude),
      '#description' => $this->t('Forms which should not have the keysave capability added, each on a separate line.'),
      '#required' => FALSE,
    ];
    $form['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

}
